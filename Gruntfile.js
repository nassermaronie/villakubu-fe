var cssJSONcustom = require("./development/css-custom.json"),
	cssJSONmobile = require("./development/css-mobile.json"),
	jsJSON = require("./development/js.json"),
	css_C_arr = [],
	css_M_arr = [],
	js_arr = ['development/js/init.js'];


function loopArr(obj, arr){

	for(var i = 0; i < obj.length; i++){
		arr.push(obj[i].file);
		console.log(obj[i].name +" loaded\n");
	}

	return arr;
}

var css_C_list = loopArr(cssJSONcustom, css_C_arr),
	css_M_list = loopArr(cssJSONmobile, css_M_arr),
	js_list = loopArr(jsJSON, js_arr);

	js_list.push('development/js/jquery-ready.js');



module.exports = function (grunt) {
	grunt
		.initConfig({
			concat: {

/*
==========================================================================================

JAVASCRIPT

==========================================================================================
*/
			  js: {
			  	options: {
			  		banner: '/*==============================================================================================\n\n\nvillakubu.js 1.0.0\n\nhttps://github.com/firstpersoncode/villa-kubu\n\n\nDear fellow developers,\n\n\nTo avoid any conflict and messy coding,\nplease note that, from now on,\nvillakubu.js cannot be downloaded anymore from its hosting.\n\n\nvillakubu.js is now being developed using task runner GRUNTjs\nas small chunks of javascript files, then concatenated into one file.\n\n\nIf you would like to make adjustments on villakubu.js,\nyou can become a collaborator to its repository\nat https://github.com/firstpersoncode/villa-kubu\n\n\nsend your username, full name or email address of your github.com account\nto nasser@softwareseni.com to be able to become collaborator of its repository.\n\n\nNasser Maronie\nFront-End Developer\n\n\nnasser@softwareseni.com\nsoftwareseni.com\n\n\nSOFTWARESENI\n\n\n==============================================================================================*/\n'
			  	},
			    src: js_list,
			    dest: 'output/js/villakubu.js',//output file (do not change)
			  },

/*
==========================================================================================

CSS FOR DESKTOP

==========================================================================================
*/
			  cssCustom: {
			  	options: {
			  		banner: '/*==============================================================================================\n\n\nvillakubu-custom.css 1.0.0\n\nhttps://github.com/firstpersoncode/villa-kubu\n\n\nDear fellow developers,\n\n\nTo avoid any conflict and messy coding,\nplease note that, from now on,\nvillakubu-custom.css cannot be downloaded anymore from its hosting.\n\n\nvillakubu-custom.css is now being developed using task runner GRUNTjs\nas small chunks of css files, then concatenated into one file.\n\n\nIf you would like to make adjustments on villakubu-custom.css,\nyou can become a collaborator to its repository\nat https://github.com/firstpersoncode/villa-kubu\n\n\nsend your username, full name or email address of your github.com account\nto nasser@softwareseni.com to be able to become collaborator of its repository.\n\n\nNasser Maronie\nFront-End Developer\n\n\nnasser@softwareseni.com\nsoftwareseni.com\n\n\nSOFTWARESENI\n\n\n==============================================================================================*/\n'
			  	},
			    src: css_C_list,
			    dest: 'output/css/villakubu-custom.css',//output file (do not change)
			  },
/*
==========================================================================================

CSS FOR MOBILE

==========================================================================================
*/
			  cssMobile: {
			  	options: {
			  		banner: '/*==============================================================================================\n\n\nvillakubu-mobile.css 1.0.0\n\nhttps://github.com/firstpersoncode/villa-kubu\n\n\nDear fellow developers,\n\n\nTo avoid any conflict and messy coding,\nplease note that, from now on,\nvillakubu-mobile.css cannot be downloaded anymore from its hosting.\n\n\nvillakubu-mobile.css is now being developed using task runner GRUNTjs\nas small chunks of css files, then concatenated into one file.\n\n\nIf you would like to make adjustments on villakubu-mobile.css,\nyou can become a collaborator to its repository\nat https://github.com/firstpersoncode/villa-kubu\n\n\nsend your username, full name or email address of your github.com account\nto nasser@softwareseni.com to be able to become collaborator of its repository.\n\n\nNasser Maronie\nFront-End Developer\n\n\nnasser@softwareseni.com\nsoftwareseni.com\n\n\nSOFTWARESENI\n\n\n==============================================================================================*/\n'
			  	},
			    src: css_M_list,
			    dest: 'output/css/villakubu-mobile.css',//output file (do not change)
			  },
			},
/*
==========================================================================================

do not change

==========================================================================================
*/
			cssmin: {
			  target: {
			    files: [{
			      expand: true,
			      cwd: 'output/css',
			      src: ['*.css', '!*.min.css'],
			      dest: 'output/css',
			      ext: '.min.css'
			    }]
			  }
			},
			uglify: {
			    my_target: {
			      files: {
			        'output/js/villakubu.min.js': ['output/js/villakubu.js']
			      }
			    }
			  },
		  	watch: {
			  js: {
			    files: ['development/js/**/*.js'],
			    tasks: ['concat:js', 'uglify'],
			  },
			  css: {
			    files: ['development/css/**/*.css', 'sass/**/*.scss'],
			    tasks: ['concat:cssCustom', 'concat:cssMobile', 'cssmin'],
			  },
			}
		});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', ['concat:js', 'concat:cssCustom', 'concat:cssMobile', 'cssmin', 'uglify', 'watch']);
};