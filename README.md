# VILLAKUBU
javascript files and css files for villakubu

https://github.com/firstpersoncode/villa-kubu

>Dear fellow developers,

>To avoid any conflict and messy coding,
please note that, from now on,
*villakubu.js*, *villakubu-custom.css*, and *villakubu-mobile.css* cannot be downloaded anymore from their hosting.


>They are now being developed using task runner GRUNTjs
as small chunks of css and js files, then concatenated into one file.


>If you would like to make adjustments on them,
you can become a collaborator to their repository
at https://github.com/firstpersoncode/villa-kubu


>Send your username, full name or email address of your github.com account
to nasser@softwareseni.com to be able to become collaborator of the repository.


## Getting Started

####Require :

###[Node js](https://nodejs.org/en/)
If you haven't used [Node](https://nodejs.org/en/) before, be sure to check out the [Getting Started](https://www.tutorialspoint.com/nodejs/nodejs_quick_guide.htm) guide.


###[Grunt js](http://gruntjs.com/)
If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide.

* Clone or download this repository.

* Open your `shell` that has been set up for using [Node](https://nodejs.org/en/)

* Type
```shell
cd directory/to/project
```

```shell
npm install
```

```shell
grunt
```


## Start Developing

* Open ```development``` folder.

* ```css``` folder is for *CSS* files.

* ```js``` folder is for *JS* files.


## Add New File

* Save file inside ```development```.

* for *js*, open *js.json*, and add your file inside array as a JSON object.
```javascript
  {
    "name":"some-name",
    "file":"development/js/some-file.js"
  }
```

* for *css*, open *css-custom.json*, and add your file inside array as a JSON object.
```javascript
  {
    "name":"some-name",
    "file":"development/css/some-file.css"
  }
```

* for *css* for mobile, open *css-mobile.json*, and add your file inside array as a JSON object.
```javascript
  {
    "name":"some-name-mobile",
    "file":"development/css/some-file-mobile.css"
  }
```


## Output files
All changes and new files will be concatenated into one file inside ```output``` folder.

For javascript files will be inside ```output/js/villakubu.js``` 

For css files for desktop will be inside ```output/js/villakubu-custom.css```

For css files for mobile will be inside ```output/js/villakubu-mobile.css```


## !IMPORTANT
>Always create **backup** files before you start make a changes. You can create ```backup``` folder inside project and save your backup files inside ```backup``` folder, .gitignore will ignore ```backup``` folder.


>*Nasser Maronie*

>*Front-End Developer*

nasser@softwareseni.com

[SOFTWARESENI](http://www.softwareseni.com/)
