/*===================================================================================================================================

Nasser Maronie
Villakubu
SoftwareSeni 2016

contrib: Nasser
custom-new.js
===================================================================================================================================*/
    newCustom : {
        init: function($){
            Villakubu.newCustom.datePickerModal();
            Villakubu.newCustom.enquireAgent();
            Villakubu.newCustom.inquiryWhiteBg();
            Villakubu.newCustom.bugReview();
            Villakubu.newCustom.bugDialogInquirySlider();
            Villakubu.newCustom.wrapInquiry();
            Villakubu.newCustom.objectFitPolyFill();
            Villakubu.newCustom.dropdownCurrency();
        },
        datePickerModal: function() {
            if(jQuery("body").hasClass("page-template-template_agent")){
                            
                jQuery('.page-template-template_agent .btn-enquire').live('click',function(e){
                    var targetModal = jQuery(this).attr('data-target');

                    jQuery("#ui-datepicker-div").appendTo(targetModal);
                    
                });
            }else{
                return false;
            }
        },
        enquireAgent: function() {
            jQuery('#agentInquiry').appendTo('.availability-calendar .container .luxelet-calendar');
        },
        inquiryWhiteBg: function() {
            if(jQuery('body').hasClass('page-id-1057')){
                if(jQuery('.entry').find('.enquiry-form').length == 1){
                    
                    jQuery('body.page-id-1057 #content>.container').css({
                        'background':'none',
                        'box-shadow':'none'
                    });

                    jQuery('body.page-id-1057 #content').css({
                        'background':'#fff'
                    });

                }else{
                    return false;
                }
            }else{
                return false;
            }
        },
        bugReview: function() {//this happen if the page being translate with translator plugin
            var reviewContainer = jQuery('#archivements .award-container>.award');

            //AJAX delegate
                var translateInit = false;
                reviewContainer.on("DOMSubtreeModified",function(){
                  if (!translateInit) {
         
                        setTimeout(function() {
                            console.log('translated')
                            
                            reviewContainer.each(function(){
                                var len = jQuery(this).find('a');
                                len.appendTo(jQuery(this));
                            });


                        }, 1000);
                    
                    translateInit = true;
                  }
                });     
        },
        bugDialogInquirySlider: function() {

            jQuery(document).mouseup(function (e){

                var dialogError = jQuery('#fullScreenSlideshow>.modal-dialog');

                    if (!dialogError.is(e.target) && dialogError.has(e.target).length === 0){
                        
                        dialogError.parent().modal('hide');                   
                        
                    }

            });
        },
        wrapInquiry: function() {
            if(!jQuery('#post-1057').length){
                return false
            }

            jQuery("#post-1057 .entry>div[class*='col-']").wrapAll('<div class="booking-page">');

            jQuery('#post-1057 .entry .booking-page').append('<div class="clearfix"></div>')
        },
        objectFitPolyFill: function () {
            objectFitImages();
        },
        dropdownGtranslate: function() {

            

        },
        dropdownCurrency: function() {
            jQuery('#ss_part_widget-15 .currencyselector .dropdown-menu > li >a').click(function(){
                jQuery('#ss_part_widget-15 .currencyselector .dropdown-menu > li >a.current-currency').removeClass('current-currency');
                jQuery(this).addClass('current-currency');
            });
        }
    },
/*===================================================================================================================================

END custom-new.js

===================================================================================================================================*/
//
//
//
//
//
//
//
//
//
//
