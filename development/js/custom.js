/*===================================================================================================================================

Nasser Maronie
Villakubu
SoftwareSeni 2016

contrib: Nasser, Yanuar
custom.js
===================================================================================================================================*/
    customApp : {
        init: function($){
            Villakubu.customApp.dataTextGallery();
            Villakubu.customApp.searchDropdown();
            Villakubu.customApp.megaMenu();
            Villakubu.customApp.stickySearch();
            // Villakubu.customApp.searchResultFilter();
            Villakubu.customApp.serviceMore();
            Villakubu.customApp.vrBug();
            Villakubu.customApp.sideBarFix();
            Villakubu.customApp.mapHighlight();
            Villakubu.customApp.inquiryStyling();
            // Villakubu.customApp.buttonSearch();
            Villakubu.customApp.fixBugModal();
            Villakubu.customApp.initHoverdir();
            // Villakubu.customApp.contactUs();
            //Villakubu.customApp.footerSscDisable();
            Villakubu.customApp.loginFunction();
            Villakubu.customApp.searchWidgetLoad();       
        },
        searchWidgetLoad: function() {
            jQuery('.sticky-search-container').css({
                'opacity':'1',
                'cursor':'default'
            });
        }, 
        dataTextGallery: function() {
            var eldataText = jQuery('.gallery-grid .row a:nth-of-type(5)');
            var dataText = eldataText.attr('data-text');
            eldataText.append('<span>'+dataText+'</span>');
        },
        searchDropdown: function() {
            jQuery('.ss-villa-search-widget .search-dropdown li:nth-of-type(2), .ss-villa-search-widget .search-dropdown li:nth-of-type(3), .ss-villa-search-widget .search-dropdown li:nth-of-type(4)').wrapAll('<ul id="custom-select-step" class="sub-menu"></ul>');
            var mainmenU = jQuery('.ss-villa-search-widget .search-dropdown li:nth-of-type(1)');        
            jQuery('body').append(jQuery('#custom-select-step'));
            mainmenU.addClass('parent-list');

            jQuery(document).mouseup(function (e){
                var container = jQuery('#custom-select-step');
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    container.fadeOut(150,function(){
                        jQuery('.search-dropdown').removeClass('open-sub');
                    });                
                }
            });        
             
            jQuery('.search-dropdown').click(function(){            
                jQuery(this).toggleClass('open-sub');

                if(jQuery(this).hasClass('open-sub')){
                    jQuery('#custom-select-step').fadeIn(150);
                }else{
                    jQuery('#custom-select-step').fadeOut(150); 
                }    
            });

            jQuery(".select-step").each(function(){
                jQuery(this).selectStep({
                  incrementLabel: "+",
                  decrementLabel: "-",
                  onChange: function(value) {
                    // debug only
                    // console.log(value, "value");
                    var stepAdult = jQuery('select[name="search-adults"]').parent().find('.selectStepValue').attr('data-value'),
                    stepKid = jQuery('select[name="search-children"]').parent().find('.selectStepValue').attr('data-value'),
                    stepRoom = jQuery('select[name="search-rooms"]').parent().find('.selectStepValue').attr('data-value'),

                    hiddenAdults = jQuery('input[name="search-adults"]'),
                    hiddenKids = jQuery('input[name="search-children"]'),
                    hiddenRooms = jQuery('input[name="search-rooms"]'),
                    
                    numRooms = jQuery('span.num-rooms'),
                    numAdults = jQuery('span.num-adults'),
                    numKids = jQuery('span.num-children')

                    hiddenAdults.attr('value',stepAdult);
                    hiddenKids.attr('value',stepKid);

                    // handle label 
                    keyRoom = jQuery('select[name="search-rooms"]').parent().find('.selectStepValue').text();
                    hiddenRooms.attr('value',keyRoom);
                    
                    if ( stepAdult === '1' ) {
                        jQuery('span.adults-label-wrapper').text('Adult');
                    } else {
                        jQuery('span.adults-label-wrapper').text('Adults');
                    }

                    if (keyRoom==='1') {
                        jQuery('span.bedrooms-label-wrapper').text('Bedroom');
                    } else {
                        jQuery('span.bedrooms-label-wrapper').text('Bedrooms');
                    }
                    
                    if (stepKid==='1') {
                        jQuery('span.children-label-wrapper').text('Child');
                    } else {
                        jQuery('.children-label-wrapper').text('Children');
                    }

                    // set text
                    numRooms.text(keyRoom);
                    
                    // if (stepRoom==='10' || stepKid==='10' || stepAdult==='8'){
                    //     jQuery('.parent-list').css('font-size','12px');
                    // }else{
                    //     jQuery('.parent-list').css('font-size','12px');
                    // }

                    numAdults.text(stepAdult);
                    numKids.text(stepKid);
                  }
                });
            });


        },
        megaMenu: function() {
            
            jQuery('#mega-menu-primary li.mega-menu-item-has-children>a').each(function(){
                jQuery(this).removeAttr('href');
            });

            var megaToggle = jQuery('#mega-menu-primary li a'),
            href;

            megaToggle.on('click',function(e){
                //if(jQuery('body').hasClass('is-mobile')){
                    if(jQuery(this).parent().hasClass('mega-menu-item-has-children') ){
                        if(!jQuery(this).parent().hasClass('open')){
                            jQuery(this).parent().addClass('open');
                            return false;
                        }else{
                            jQuery(this).parent().removeClass('open');
                        }
                    }                    
                //}

               else if(!jQuery(this).parent().is('#mega-menu-item-152')){
                    /*disable ssc gallery on megamenu*/
                    if(jQuery(this).parent().is('#mega-menu-item-1446')){                    
                        href = jQuery(this).attr('href')+"?ssc_disable=1";
                        console.log(href + " disable ss-combiner for this page only !");
                        location.href = href;
                    }else{
                        href = jQuery(this).attr('href');
                        location.href = href;
                    }                
                }
            });
        },
        stickySearch: function() {
            
            var searchContainer = jQuery('#home-top').find('.sticky-search-container'),
            subSearchContainer = jQuery('#custom-select-step'),
            theOffset = jQuery('#home-middle');

            if (!jQuery('body').hasClass('home')) { return false; }        

            jQuery(window).scroll(function(){

                var hT = theOffset.offset().top;

                if(jQuery(this).scrollTop() > hT){
                    jQuery('body').addClass('sticky-search-on');
                    searchContainer.addClass('sticky-search search');
                    jQuery('#ui-datepicker-div').addClass('search');
                    subSearchContainer.addClass('sticky-sub-search search');  
                }
                else{
                    jQuery('body').removeClass('sticky-search-on');
                    searchContainer.removeClass('sticky-search search mobile-sticky');
                    jQuery('#ui-datepicker-div').removeClass('search');
                    subSearchContainer.removeClass('sticky-sub-search search');
                }
            });

            searchContainer.find('form').click(function(){
                searchContainer.addClass('mobile-sticky');
            });

            jQuery(document).mouseup(function (e){

                var container = jQuery('.search');

                if (!container.is(e.target) && container.has(e.target).length === 0){
                    searchContainer.removeClass('mobile-sticky');
                }

            });

        },
        searchResultFilter: function() {
            if(jQuery('body').hasClass('page-id-116')){
                filterPrepend();
            }else{
                return false;
            }

            function filterPrepend(){
                var filterForm = jQuery('.search-param');
                
                jQuery('#content').prepend(filterForm);
                var replaceDiv = filterForm.next();

                if(replaceDiv.length !== 0){
                    replaceDiv.insertBefore(filterForm);
                    filterForm.addClass('container');
                    filterForm.wrapAll('<div class="filter-parent"></div>');
                }
                
            }
        },
        serviceMore: function() {
            var clicK = 0;
            jQuery('#read-more-services-trigger').click(function(e){            
                e.preventDefault();
                
                    clicK++;
                    switch(clicK) {
                        case 1:
                            jQuery(this).html('Read Less');
                            jQuery(this).parent().fadeOut(300,function(){
                                jQuery(this).fadeIn(300);
                                jQuery(this).addClass('more');
                            });
                            break;
                        case 2:
                            jQuery(this).html('Read More');
                            jQuery(this).parent().fadeOut(300,function(){
                                jQuery(this).fadeIn(300);
                                jQuery(this).removeClass('more');
                            });     
                            clicK = 0;
                            break;
                    }           
            });

            var custReview = jQuery('.customer-review'),
            custChild = custReview.find('.review-wrap');

            if (custChild.height() < 457) {
                custReview.find('.view-all').hide();
            }else{
                custReview.find('.view-all').show();
            }

            var custService = jQuery('.services'),
            serChild = custService.find('.service-wrap');

            if (serChild.height() < 200) {
                custService.find('.view-all').hide();
            }else{
                custService.find('.view-all').show();
            }
        },
        vrBug : function() {

            function getRootUrl() {
                return window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/';
            }

            var vrShow = jQuery('.vr-icon, .btn-vr');
            var panO = jQuery('.vr-content');

            // VR function
            panO.append('<i class="fa fa-times-circle close" aria-hidden="true"></i>');     

            vrShow.click(function(){

                // prepare variables
                var vr_index = jQuery(this).attr('data-index') ? jQuery(this).attr('data-index') : 0;
                var vr_prev = jQuery('.btn-prev');
                var vr_next = jQuery('.btn-next');
                // remove old vr data if any
                jQuery('.vr-content').children().not('.fa-times-circle.close, .btn-vr').remove();

                // prepare ajax data
                var vr_ajaxdata = {
                    'action': 'ss_single_villa_vr',
                    'post_id' : jQuery('input[name="post_id"]').val(),
                    'index' : vr_index
                };

                panO.animate({top:'0px',opacity:'1'},300,function(){
                    jQuery(this).css({"pointer-events":"auto"});
                });
               
                // pass url value to AJAX
                jQuery.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: vr_ajaxdata,
                    success: function(response) {
                        panO.append(response);
                        console.log('ajax loading');
                        console.log(this.url+" "+this.data);
                        // handle button
                        if (vr_index > 0 ) { 
                            vr_next.hide(); 
                            vr_prev.text('Click to view '+jQuery('input[name="vr-prev"]').val());
                            vr_prev.show();
                        } else {
                            vr_prev.hide(); 
                            vr_next.text('Click to view '+jQuery('input[name="vr-next"]').val());
                            vr_next.show();
                        }
                    },
                    complete: function() {
                        panO.find('.ggskin_text').html('<div class="load-ajax"><img src="'+getRootUrl()+'wp-content/uploads/2016/08/loading1.gif" alt="loading..."></div>');
                        console.log('ajax loaded');

                    },
                    error: function() {
                        panO.html("<div class='error-ajax'>Error load VR</div>");
                        console.log('ajax error');
                    }
                });
            });
            
            //close VR
            jQuery('.close').click(function(){
                panO.animate({top:'-1000px',opacity:'0'},300,function(){
                    jQuery(this).css({"pointer-events":"none"});
                });
            });
        },
        sideBarFix : function() {  

            if(jQuery('body').hasClass('page-id-79') || jQuery('body').hasClass('blog') && jQuery('body').hasClass('not-mobile') || jQuery('body').hasClass('single-post') && jQuery('body').hasClass('not-mobile')){
                var sideBar = jQuery('.sticky-bar'),
                entryRev = jQuery('.sticky-target');

                if(jQuery('#page-sidebar').length){
                    sideBar.find('aside').wrapAll('<div>');
                }
                

                jQuery(window).scroll(function(){

                    if(jQuery(this).scrollTop() > entryRev.offset().top && jQuery(this).scrollTop() < (entryRev.height() + entryRev.offset().top) - 700){
                        sideBar.addClass('scroll-active');
                        sideBar.css({
                            '-ms-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            '-o-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            '-moz-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            '-webkit-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            'transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)'
                        });                   
                    }
                    else if(jQuery(this).scrollTop() > entryRev.offset().top && jQuery(this).scrollTop() > (entryRev.height() + entryRev.offset().top) - 700){
                        sideBar.removeClass('scroll-active');
                        sideBar.css({
                            '-ms-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            '-o-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            '-moz-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            '-webkit-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            'transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)'
                        });
                    }
                    else{
                        sideBar.removeClass('scroll-active');
                        sideBar.css({
                            '-ms-transform':'translateY(0)',
                            '-o-transform':'translateY(0)',
                            '-moz-transform':'translateY(0)',
                            '-webkit-transform':'translateY(0)',
                            'transform':'translateY(0)'
                        });
                    }
                });
            }else{
                return false;
            }
        },
        mapHighlight: function() {
            $.fn.maphilight.defaults = {
                fill: false,
                fillColor: 'ffffff',
                fillOpacity: 0.4,
                stroke: false,
                strokeColor: 'transparent',
                strokeOpacity: 1,
                strokeWidth: 2,
                fade: true,
                alwaysOn: false,
                neverOn: false,
                groupBy: false,
                wrapClass: true,
                shadow: true,
                shadowX: 0,
                shadowY: 0,
                shadowRadius: 15,
                shadowColor: 'ffffff',
                shadowOpacity: 1,
                shadowPosition: 'outside',
                shadowFrom: false
            }

            //init interactive map
            jQuery('img[usemap]').maphilight();

            var mapImage = jQuery('#ImageMapsCom-villakubu-floorplan'),
            mapArea = mapImage.find('area'),
            tolltipParent = jQuery('.floorplan-container'),
            tooltipContainer = jQuery('.floorplan-container>div'),
            tooltipActive = jQuery('.floorplan-container .active-hover');

            tooltipContainer.prepend('<i class="fa fa-times-circle hover-close"></i>');
            tooltipContainer.prepend('<i class="fa fa-expand resize"></i>');

            //larger screen for dragable
            if(jQuery(window).width() > 767){
                tolltipParent.draggable({
                    cursor: 'move',
                    containment: 'document',
                    snap: '#content',
                    start: function () {
                        tolltipParent.css({right:'auto'});
                    }
                });
            }

            mapArea.on('mouseenter',function(){
                var thisAttr = jQuery(this).attr('data-hover');
                hideEl();
                jQuery('#'+thisAttr).addClass('active-hover');
            });

            jQuery('.resize').on('click',resizeEl);

            jQuery('.hover-close').on('click',hideEl);

            jQuery(document).mouseup(function (e){
                var container = jQuery('.floorplan-container>div');
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    hideEl();
                }
            });

            //resize dialog
            function resizeEl() {
                tolltipParent.toggleClass('zoomed');
                tooltipContainer.find('img').toggleClass('zoomed');
            }
            
            //hide dialog
            function hideEl() {
                tooltipContainer.removeClass('active-hover');
                tolltipParent.css({top:'0',left:'auto',right:'0'});
                tolltipParent.removeClass('zoomed');
                tooltipContainer.find('img').removeClass('zoomed');
            }

            //force canvas height
            jQuery('.vl-floorplan-layout canvas').css({
                height : 'auto'
            });
        },
        inquiryStyling: function() {
            if(jQuery('body').hasClass('page-id-1057')){
                // jQuery('#post-1057 #bapi-inquiryform').addClass('col-sm-10');
                jQuery('.flex-direction-nav>li>a').empty();
            }else{
                return false;
            }
        },
        buttonSearch: function() {
            if(jQuery('body').hasClass('page-template-template_search_villas')){
                jQuery('.filter>div>form').prepend('<div class="button-category"></div>');
                var buttonFilter = jQuery('.field-wrapper:not(:last-child)');

                buttonFilter.show(1,function(){
                    jQuery(this).appendTo(".button-category");
                    jQuery(this).css('display','inline-block');
                });
                

            }else{
                return false;
            }
        },
        fixBugModal: function() {

            function dialogTrigger(el){
                var thisEl = jQuery(el);
                var trigger = jQuery('*[data-toggle="ss-modal"]');
                var close = thisEl.find('.modal-close');

                thisEl.on('show.bs.modal',function(){
                    if(!jQuery('body').hasClass('ss-modal-show')){
                        jQuery('body').addClass('ss-modal-show');
                    }                
                });

                thisEl.on('hide.bs.modal',function(){
                    jQuery('body').removeClass('ss-modal-show');
                });

                trigger.live('click',function(e){
                    e.preventDefault();
                    var target = jQuery(this).attr('data-target');
                    var dialog = jQuery('.ss-modal-dialog').find(target).parent();
                    if(!dialog.hasClass('active')){
                        dialog.addClass('active');
                        thisEl.trigger('show.bs.modal',this);
                    }else{
                        return false;
                    }
                });

                close.live('click',function(){
                    jQuery(this).parents('.ss-modal-dialog').removeClass('active');
                    thisEl.trigger('hide.bs.modal',this);
                });

                jQuery(document).mouseup(function (e){

                    var container = jQuery('.ss-modal');

                    if(container.attr('id') === "enquireModal"){
                        return false;
                    }

                    if (!container.is(e.target) && container.has(e.target).length === 0){
                        jQuery('.ss-modal-dialog').removeClass('active');
                        thisEl.trigger('hide.bs.modal',this);
                    }

                });
            };        

            dialogTrigger('.ss-modal');

            //AJAX delegate
            var bapiInit = false;
            jQuery("#bookingform").on("DOMSubtreeModified",function(){
              if (!bapiInit) {
        
                    setTimeout(function() {
                        var elm = jQuery('#revisedates').parent();
                        var checkIn = elm.find('#makebookingcheckin');

                        if(checkIn.val() == ""){
                            if(!elm.hasClass('active')){
                                elm.addClass('active');
                                elm.trigger('show.bs.modal',this);
                            }else{
                                    return false;
                            }
                        }else{
                            return false;
                        }

                         
                    }, 1000);
                
                bapiInit = true;
              }
            });
        },
        initHoverdir: function() {

            if(jQuery('body').hasClass('page-id-64')){
                
                jQuery('.gallery>dl>dt').find('a').append('<div>');
                            
                jQuery('.gallery>dl>dt').each(function(){
                    jQuery(this).hoverdir({
                        speed: 150,
                        easing: 'ease',
                        hoverDelay: 0
                    });
                });

            }else{
                return false;
            }
        },
        contactUs: function() {


            if(jQuery('body').hasClass('page-id-347')){

                var bapiContain = jQuery('#bapi-inquiryform'),
                ul = bapiContain.find(".unstyled"),
                input = ul.find("li>input.required");

                input.on('focus',function(){
                    jQuery(this).removeClass('error');
                    jQuery(this).parent().removeClass('error');
                });

                input.on('blur',function(){
                    if(jQuery(this).val()){
                        jQuery(this).removeClass('error');
                        jQuery(this).parent().removeClass('error');
                    }                
                });

                jQuery('#btn-leadrequest').click(function(e) {
                    var isValid = true;
                    input.each(function() {
                        if (jQuery.trim(jQuery(this).val()) == '') {
                            isValid = false;
                            jQuery(this).addClass('error');
                            jQuery(this).parent().addClass('error');
                        }
                        else {
                            jQuery(this).removeClass('error');
                            jQuery(this).parent().removeClass('error');
                        }
                    });
                    if (isValid == false){
                        e.preventDefault();                   
                    }
                    
                    ga('send', {
                          hitType: 'event',
                          eventCategory: 'Enquiry Form',
                          eventAction: 'enquiryclick',
                          eventLabel: 'Enquiry sent'
                        });
                    
                    //ga('send', 'event', 'Enquiry Form', 'enquiryclick', 'Enquiry sent');

                    var email = jQuery("input#txtEmail").val();  
                    if (!validEmail(email)) {
                        jQuery("input#txtEmail").addClass('error');
                        jQuery("input#txtEmail").parent().addClass('error'); 
                    } 

                });
            }else{
                return false;
            }
        },
        footerSscDisable: function() {
            var nossc = jQuery('#menu-item-82>a');
            var nosscHref; 
            /*disable ssc gallery on footer*/
            nossc.on('click',function(e){
                return false;
                nosscHref = nossc.attr('href')+"?ssc_disable=1";
                console.log(nosscHref + " disable ss-combiner for this page only !");
                location.href = nosscHref;
            });
        },
        loginFunction: function() {
            if (jQuery('body').hasClass('page-id-2149')){
                jQuery('form#loginform input[type="text"]').attr('placeholder','Username or Email');
                jQuery('form#loginform input[type="password"]').attr('placeholder','Password');

                var input = jQuery('form#loginform input:not([type="submit"])');

                input.on('focus',function(){
                    jQuery(this).parent().removeClass('error');
                });

                input.on('blur',function(){
                    if(jQuery(this).val()){
                        jQuery(this).parent().removeClass('error');
                    }                
                });

                jQuery('#wp-submit').click(function(e) {
                    
                    var isValid = true;
                    input.each(function() {
                        if (jQuery.trim(jQuery(this).val()) == '') {
                            isValid = false;
                            jQuery(this).parent().delay(1000).addClass('error');
                        }
                        else {
                            jQuery(this).parent().delay(1000).removeClass('error');
                        }
                    });
                    if (isValid == false){
                        e.preventDefault();                   
                    } 

                });
            }else{
                return false;
            }
        }
    },
/*===================================================================================================================================

END custom.js

===================================================================================================================================*/
//
//
//
//
//
//
//
//
//
//
