	handleGtranslate: function() {

		(function($){
			$.fn.gtranslate = function(option){
			    
		        var settings = $.extend({
		        		source: $("#gtranslate"),
			            fakeDropdown: $("<div></div>"),
				    	callback: function(){}//callback
		        }, option);

		        var googtrans = getCookie("googtrans");

		        if (googtrans) {
		          googtrans = googtrans.split('/')[2];
		        } else {
		          googtrans = settings.source.val().split('|')[1];
		        }

		        settings.source.val('en|'+googtrans);

		        var selected = settings.source.find("option[value='en|"+googtrans+"']"),
		        	options = $("option", settings.source);

		        return this.each(function(){  
		        	var el = $(this);
		        	el.append(settings.fakeDropdown);

		        	settings.fakeDropdown.append(
		        		'<a>'+
		        			'<span class="gtranslate-label">Language</span><img src="'+app.homeURL+'/wp-content/themes/boost/img/flag/' + googtrans + '.png" /><span class="value">' + selected.val() + '</span><i class="fa fa-chevron-down"></i>'+	        			
		        		'</a>'+
		        		'<ul></ul>');
		        	options.each(function(){
        		    	settings.fakeDropdown.find('ul').append('<li><a><img src="'+app.homeURL+'/wp-content/themes/boost/img/flag/'+$(this).val().split('|')[1]+'.png" />' + 
        		        '<span>'+ $(this).text() +'</span>'+'<span class="value notranslate">' + 
        		        $(this).val() + '</span></a></li>');
        		    });
        		    settings.fakeDropdown.find("a:first").click(function() {
        		        $(this).parent().find("ul").fadeToggle(200);
        		    });
        		    settings.fakeDropdown.find("ul").find("a").click(function(){
        		    	$("ul li a.current-lang").removeClass("current-lang");
        		    	$(this).addClass("current-lang");
        		    	console.log(googtrans)
        		    	settings.callback();
        		    });
        		    $(document).mouseup(function (e){
        		        var dropdown = settings.fakeDropdown.find("ul");
    		            if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0){        		                
    		                dropdown.fadeOut(300); 
    		            }
        		    });
		        });		        
			}
		}(jQuery));

		$("#gtranslate").gtranslate({
			source: $("#gtranslate select:first"),
			fakeDropdown : 	$('<div id="target" class="dropdown"></div>'),
			callback: function(){
				console.log('callback');
			}
		});
		$("#gtranslate > .dropdown > ul > li > a").click(function() {
		   var text = '<span class="gtranslate-label">Language</span>'+ $(this).html() +'<i class="fa fa-chevron-down"></i>';
		   $("#gtranslate .dropdown > a").html(text);
		   $("#gtranslate .dropdown > ul").fadeOut(200);
		    
		   var source = $("#gtranslate select:first");
		   var new_lang = $(this).find("span.value").html();
		   if (source.val()!=new_lang) {
		     source.val(new_lang).change();
		   }
		});
	},