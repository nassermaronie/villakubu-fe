	widgetOnSearchPosition: function(){
		(function($){

			$.fn.responsivePosition = function(option){
				var settings = $.extend({
					offsetTarget: jQuery('.ss-villa-search-widget'),
					elTarget: jQuery('#custom-select-step'),
					onDesktop: 400,
					onMobile: 200,
					elCloseToBot: {},
					elCloseToTop: {},
					elOnStick: {},
					stickySearch: null,
					targetSticky: null
				}, option);

							

				return this.each(function(){			
					if(settings.targetSticky){
						var sticky_target = settings.targetSticky;
					}

					var desktop = settings.offsetTarget.offset().top > (jQuery(window).scrollTop() + jQuery(window).height() - settings.onDesktop),					
						mobile = settings.offsetTarget.offset().top > (jQuery(window).scrollTop() + jQuery(window).height() - settings.onMobile);

					if(jQuery('body').hasClass('is-mobile')){
						if(mobile){	
							settings.elTarget.css(settings.elCloseToBot);
						}else{
							settings.elTarget.css(settings.elCloseToTop);							
							if(settings.stickySearch == true){
								if(sticky_target){settings.elTarget.css(settings.elOnStick)}
								else{settings.elTarget.css(settings.elCloseToTop);}
							}							
						}						
					}else{
						if(desktop){
							settings.elTarget.css(settings.elCloseToBot);
						}else{
							settings.elTarget.css(settings.elCloseToTop);				
							if(settings.stickySearch == true){
								if(sticky_target){settings.elTarget.css(settings.elOnStick)}
								else{settings.elTarget.css(settings.elCloseToTop);}
							}							
						}
					}
				});
			}
		}(jQuery));

		var selectStep = jQuery('#custom-select-step'),
			targetSelectStep = jQuery('.ss-villa-search-widget .search-dropdown');

		targetSelectStep.on('click',function(){
			jQuery(this).responsivePosition({	
				elCloseToBot:{
					'top':targetSelectStep.offset().top - selectStep.outerHeight(),
			        'left':targetSelectStep.offset().left +10.5,
			        'width':targetSelectStep.width()
				},
				elCloseToTop:{
					'top':targetSelectStep.offset().top +38,
			        'left':targetSelectStep.offset().left +10.5,
			        'width':targetSelectStep.width()
				},
				stickySearch: true,
				targetSticky: targetSelectStep.parents('.sticky-search-container').hasClass('sticky-search'),
				elOnStick: {
					'top':(targetSelectStep.offset().top +38) - jQuery(window).scrollTop(),
			        'left':targetSelectStep.offset().left +10.5,
			        'width':targetSelectStep.width()
				}
			});
		});
		
		jQuery('#gtranslate .dropdown a:first').on('click',function(){
			jQuery(this).responsivePosition({
				elTarget: jQuery('#gtranslate .dropdown ul'),			
				elCloseToBot:{'top':'auto','bottom':'30px'},
				elCloseToTop:{'top':'27.5px','bottom':'auto'},
				stickySearch: false
			});
		});
		
		jQuery('.currency-wrapper .dropdown-toggle').on('click',function(){
			jQuery(this).responsivePosition({
				elTarget: jQuery('.currency-wrapper .dropdown-menu'),	
				elCloseToBot:{'top':'auto','bottom':'26px'},
				elCloseToTop:{'top':'27px','bottom':'auto'},
				stickySearch: false
			});
		});
	},