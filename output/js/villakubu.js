/*==============================================================================================


villakubu.js 1.0.0

https://github.com/firstpersoncode/villa-kubu


Dear fellow developers,


To avoid any conflict and messy coding,
please note that, from now on,
villakubu.js cannot be downloaded anymore from its hosting.


villakubu.js is now being developed using task runner GRUNTjs
as small chunks of javascript files, then concatenated into one file.


If you would like to make adjustments on villakubu.js,
you can become a collaborator to its repository
at https://github.com/firstpersoncode/villa-kubu


send your username, full name or email address of your github.com account
to nasser@softwareseni.com to be able to become collaborator of its repository.


Nasser Maronie
Front-End Developer


nasser@softwareseni.com
softwareseni.com


SOFTWARESENI


==============================================================================================*/
/*===================================================================================================================================

Nasser Maronie
Villakubu
SoftwareSeni 2016

===================================================================================================================================*/
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

var Villakubu = {
    init: function($){
        Villakubu.hoverd();
        Villakubu.selectStep();
        Villakubu.customApp.init($);
        Villakubu.newCustom.init($);
        Villakubu.handleGtranslate();
        Villakubu.widgetOnSearchPosition();
    },
/*===================================================================================================================================
===================================================================================================================================*/
/*===================================================================================================================================

HOVER DIR PLUGIN

===================================================================================================================================*/
    hoverd: function() {


        ( function( $, window, undefined ) {
            
            'use strict';

            $.HoverDir = function( options, element ) {
                
                this.$el = $( element );
                this._init( options );

            };

            // the options
            $.HoverDir.defaults = {
                speed : 300,
                easing : 'ease',
                hoverDelay : 0,
                inverse : false
            };

            $.HoverDir.prototype = {

                _init : function( options ) {
                    
                    // options
                    this.options = $.extend( true, {}, $.HoverDir.defaults, options );
                    // transition properties
                    this.transitionProp = 'all ' + this.options.speed + 'ms ' + this.options.easing;
                    // support for CSS transitions
                    this.support = Modernizr.csstransitions;
                    // load the events
                    this._loadEvents();

                },
                _loadEvents : function() {

                    var self = this;
                    
                    this.$el.on( 'mouseenter.hoverdir, mouseleave.hoverdir', function( event ) {
                        
                        var $el = $( this ),
                            $hoverElem = $el.find( 'div' ),
                            direction = self._getDir( $el, { x : event.pageX, y : event.pageY } ),
                            styleCSS = self._getStyle( direction );
                        
                        if( event.type === 'mouseenter' ) {
                            
                            $hoverElem.hide().css( styleCSS.from );
                            clearTimeout( self.tmhover );

                            self.tmhover = setTimeout( function() {
                                
                                $hoverElem.show( 0, function() {
                                    
                                    var $el = $( this );
                                    if( self.support ) {
                                        $el.css( 'transition', self.transitionProp );
                                    }
                                    self._applyAnimation( $el, styleCSS.to, self.options.speed );

                                } );
                                
                            
                            }, self.options.hoverDelay );
                            
                        }
                        else {
                        
                            if( self.support ) {
                                $hoverElem.css( 'transition', self.transitionProp );
                            }
                            clearTimeout( self.tmhover );
                            self._applyAnimation( $hoverElem, styleCSS.from, self.options.speed );
                            
                        }
                            
                    } );

                },
                _getDir : function( $el, coordinates ) {
                    
                    // the width and height of the current div
                    var w = $el.width(),
                        h = $el.height(),

                        // calculate the x and y to get an angle to the center of the div from that x and y.
                        // gets the x value relative to the center of the DIV and "normalize" it
                        x = ( coordinates.x - $el.offset().left - ( w/2 )) * ( w > h ? ( h/w ) : 1 ),
                        y = ( coordinates.y - $el.offset().top  - ( h/2 )) * ( h > w ? ( w/h ) : 1 ),
                    
                        // the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
                        // first calculate the angle of the point,
                        // add 180 deg to get rid of the negative values
                        // divide by 90 to get the quadrant
                        // add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
                        direction = Math.round( ( ( ( Math.atan2(y, x) * (180 / Math.PI) ) + 180 ) / 90 ) + 3 ) % 4;
                    
                    return direction;
                    
                },
                _getStyle : function( direction ) {
                    
                    var fromStyle, toStyle,
                        slideFromTop = { left : '0px', top : '-100%' },
                        slideFromBottom = { left : '0px', top : '100%' },
                        slideFromLeft = { left : '-100%', top : '0px' },
                        slideFromRight = { left : '100%', top : '0px' },
                        slideTop = { top : '0px' },
                        slideLeft = { left : '0px' };
                    
                    switch( direction ) {
                        case 0:
                            // from top
                            fromStyle = !this.options.inverse ? slideFromTop : slideFromBottom;
                            toStyle = slideTop;
                            break;
                        case 1:
                            // from right
                            fromStyle = !this.options.inverse ? slideFromRight : slideFromLeft;
                            toStyle = slideLeft;
                            break;
                        case 2:
                            // from bottom
                            fromStyle = !this.options.inverse ? slideFromBottom : slideFromTop;
                            toStyle = slideTop;
                            break;
                        case 3:
                            // from left
                            fromStyle = !this.options.inverse ? slideFromLeft : slideFromRight;
                            toStyle = slideLeft;
                            break;
                    };
                    
                    return { from : fromStyle, to : toStyle };
                            
                },
                // apply a transition or fallback to jquery animate based on Modernizr.csstransitions support
                _applyAnimation : function( el, styleCSS, speed ) {

                    $.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
                    el.stop().applyStyle( styleCSS, $.extend( true, [], { duration : speed + 'ms' } ) );

                },

            };
            
            var logError = function( message ) {

                if ( window.console ) {

                    window.console.error( message );
                
                }

            };
            
            $.fn.hoverdir = function( options ) {

                var instance = $.data( this, 'hoverdir' );
                
                if ( typeof options === 'string' ) {
                    
                    var args = Array.prototype.slice.call( arguments, 1 );
                    
                    this.each(function() {
                    
                        if ( !instance ) {

                            logError( "cannot call methods on hoverdir prior to initialization; " +
                            "attempted to call method '" + options + "'" );
                            return;
                        
                        }
                        
                        if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {

                            logError( "no such method '" + options + "' for hoverdir instance" );
                            return;
                        
                        }
                        
                        instance[ options ].apply( instance, args );
                    
                    });
                
                } 
                else {
                
                    this.each(function() {
                        
                        if ( instance ) {

                            instance._init();
                        
                        }
                        else {

                            instance = $.data( this, 'hoverdir', new $.HoverDir( options, this ) );
                        
                        }

                    });
                
                }
                
                return instance;
                
            };
            
        } )( jQuery, window );

    },
/*===================================================================================================================================

SELECT STEP PLUGIN

Nasser Maronie
Villakubu
SoftwareSeni 2016

contrib: Rudy
jquery-select-step.js
===================================================================================================================================*/
    selectStep: function() {

        if (typeof Object.assign != 'function') {
          (function () {
            Object.assign = function (target) {
              'use strict';
              // We must check against these specific cases.
              if (target === undefined || target === null) {
                throw new TypeError('Cannot convert undefined or null to object');
              }

              var output = Object(target);
              for (var index = 1; index < arguments.length; index++) {
                var source = arguments[index];
                if (source !== undefined && source !== null) {
                  for (var nextKey in source) {
                    if (source.hasOwnProperty(nextKey)) {
                      output[nextKey] = source[nextKey];
                    }
                  }
                }
              }
              return output;
            };
          })();
        }

        (function ($) {
            $.fn.selectStep = function (vars) {
                /*
                 * Function to get all plugin's variables
                 * and element options on array
                 */
                function getData(element, callback) {
                    // Define default variables
                    var defaultVars = {
                        onChange: null,
                        incrementLabel: "+",
                        decrementLabel: "-"
                    };
                    var assign = Object.assign;
                    // Get all plugin variables
                    vars = assign({}, defaultVars, vars);
                    var options = [];
                    // Get select options
                    var optElement = $(element).find("option");
                    optElement.each(function (i, o) {
                        var name = $(this).text();
                        var value = $(this).attr("value");
                        var selected = $(this).is(':selected');
                        options = options.concat([{ name: name, value: value, selected: selected }]);
                        if (i === optElement.length - 1) {
                            // Fire callback with select options and variables
                            callback(vars, options);
                        }
                    });
                }
                /*
                 * Function to create fake element
                 * to mock the select elements
                 */
                function addFakeElements(element, callback) {
                    getData(element, function (vars, options) {
                        // Check if options is empty
                        if (!options.length) {
                            return;
                        }
                        // Add class to select element
                        jQuery(element).addClass("select-step");
                        // Add fake elements
                        var incrementLabel = vars.incrementLabel, decrementLabel = vars.decrementLabel;
                        // Find selected option on the select element
                        var isSelected = [];
                        options.map(function (opt, key) {
                            if (opt.selected) {
                                isSelected = isSelected.concat([{ key: key, name: opt.name, value: opt.value }]);
                            }
                            return false;
                        });
                        var selectedOptionName = isSelected.length ? isSelected[0].name : null;
                        var selectedOptionKey = isSelected.length ? isSelected[0].key : null;
                        var selectedOptionvalue = isSelected.length ? isSelected[0].value : null;
                        // Create the fake element
                        var fakeElement = "<div class=\"jquery-select-step-element\">\n                  <button class=\"decrementStep\">" + decrementLabel + "</button>\n                  <div class=\"selectStepValue\" data-key=\"" + selectedOptionKey + "\" data-value=\"" + selectedOptionvalue + "\">\n                    " + selectedOptionName + "\n                  </div>\n                  <button class=\"incrementStep\">" + incrementLabel + "</button>\n                </div>";
                        // Wrap select to a div
                        var parentElement = $(element)
                            .wrap("<div class=\"jquery-select-step\"></div>")
                            .parent();
                        // Append the fake element
                        parentElement.append(fakeElement);
                        // Fire callback when finished
                        callback(vars, options, parentElement);
                    });
                }
                /*
                 * Function to check if variable is function
                 */
                function isFunction(functionToCheck) {
                    var getType = {};
                    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
                }
                /*
                 * Function to handle the increment and decrement of the plugins
                 * it fire onChange event and change the select value
                 */
                function handleChange(element, vars, options, type) {
                    var selectStepValue = $(element).find(".selectStepValue"); // Get the element that show the value
                    var key = parseInt($(selectStepValue).attr("data-key")); // Get current active key
                    var onChange = vars.onChange; // Get user onChange event
                    // Check if the key is not less than 0 or bigger than select options length
                    if (type === "decrement" && key <= 0 || type === "increment" && key >= options.length - 1) {
                        return;
                    }
                    // Get the new key
                    var newKey = (type === "decrement" ? (key - 1) : (key + 1));
                    var _a = options[newKey], name = _a.name, value = _a.value; // Get name and value of the new key
                    // Change value
                    selectStepValue.text(name);
                    selectStepValue.attr("data-key", newKey);
                    selectStepValue.attr("data-value", value);
                    // Change select element selected options
                    jQuery(element).find(".select-step option").removeAttr('selected');
                    jQuery(element).find(".select-step option:eq(" + newKey + ")").attr('selected', true);
                    // Fire onChange event
                    if (onChange !== null && isFunction(onChange)) {
                        onChange({
                            key: newKey,
                            name: name,
                            value: value
                        });
                    }
                }
                /*
                 * Function to initialize the plugins
                 */
                function init(element) {
                    addFakeElements(element, function (vars, options, parentElm) {
                        // Listen to change event
                        $(parentElm).on("click", ".decrementStep", function () {
                            handleChange(parentElm, vars, options, "decrement");
                        });
                        $(parentElm).on("click", ".incrementStep", function () {
                            handleChange(parentElm, vars, options, "increment");
                        });
                    });
                }
                init(this);
                return this;
            };
        }(jQuery));

    },
/*===================================================================================================================================

Nasser Maronie
Villakubu
SoftwareSeni 2016

contrib: Nasser, Yanuar
custom.js
===================================================================================================================================*/
    customApp : {
        init: function($){
            Villakubu.customApp.dataTextGallery();
            Villakubu.customApp.searchDropdown();
            Villakubu.customApp.megaMenu();
            Villakubu.customApp.stickySearch();
            // Villakubu.customApp.searchResultFilter();
            Villakubu.customApp.serviceMore();
            Villakubu.customApp.vrBug();
            Villakubu.customApp.sideBarFix();
            Villakubu.customApp.mapHighlight();
            Villakubu.customApp.inquiryStyling();
            // Villakubu.customApp.buttonSearch();
            Villakubu.customApp.fixBugModal();
            Villakubu.customApp.initHoverdir();
            // Villakubu.customApp.contactUs();
            //Villakubu.customApp.footerSscDisable();
            Villakubu.customApp.loginFunction();
            Villakubu.customApp.searchWidgetLoad();       
        },
        searchWidgetLoad: function() {
            jQuery('.sticky-search-container').css({
                'opacity':'1',
                'cursor':'default'
            });
        }, 
        dataTextGallery: function() {
            var eldataText = jQuery('.gallery-grid .row a:nth-of-type(5)');
            var dataText = eldataText.attr('data-text');
            eldataText.append('<span>'+dataText+'</span>');
        },
        searchDropdown: function() {
            jQuery('.ss-villa-search-widget .search-dropdown li:nth-of-type(2), .ss-villa-search-widget .search-dropdown li:nth-of-type(3), .ss-villa-search-widget .search-dropdown li:nth-of-type(4)').wrapAll('<ul id="custom-select-step" class="sub-menu"></ul>');
            var mainmenU = jQuery('.ss-villa-search-widget .search-dropdown li:nth-of-type(1)');        
            jQuery('body').append(jQuery('#custom-select-step'));
            mainmenU.addClass('parent-list');

            jQuery(document).mouseup(function (e){
                var container = jQuery('#custom-select-step');
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    container.fadeOut(150,function(){
                        jQuery('.search-dropdown').removeClass('open-sub');
                    });                
                }
            });        
             
            jQuery('.search-dropdown').click(function(){            
                jQuery(this).toggleClass('open-sub');

                if(jQuery(this).hasClass('open-sub')){
                    jQuery('#custom-select-step').fadeIn(150);
                }else{
                    jQuery('#custom-select-step').fadeOut(150); 
                }    
            });

            jQuery(".select-step").each(function(){
                jQuery(this).selectStep({
                  incrementLabel: "+",
                  decrementLabel: "-",
                  onChange: function(value) {
                    // debug only
                    // console.log(value, "value");
                    var stepAdult = jQuery('select[name="search-adults"]').parent().find('.selectStepValue').attr('data-value'),
                    stepKid = jQuery('select[name="search-children"]').parent().find('.selectStepValue').attr('data-value'),
                    stepRoom = jQuery('select[name="search-rooms"]').parent().find('.selectStepValue').attr('data-value'),

                    hiddenAdults = jQuery('input[name="search-adults"]'),
                    hiddenKids = jQuery('input[name="search-children"]'),
                    hiddenRooms = jQuery('input[name="search-rooms"]'),
                    
                    numRooms = jQuery('span.num-rooms'),
                    numAdults = jQuery('span.num-adults'),
                    numKids = jQuery('span.num-children')

                    hiddenAdults.attr('value',stepAdult);
                    hiddenKids.attr('value',stepKid);

                    // handle label 
                    keyRoom = jQuery('select[name="search-rooms"]').parent().find('.selectStepValue').text();
                    hiddenRooms.attr('value',keyRoom);
                    
                    if ( stepAdult === '1' ) {
                        jQuery('span.adults-label-wrapper').text('Adult');
                    } else {
                        jQuery('span.adults-label-wrapper').text('Adults');
                    }

                    if (keyRoom==='1') {
                        jQuery('span.bedrooms-label-wrapper').text('Bedroom');
                    } else {
                        jQuery('span.bedrooms-label-wrapper').text('Bedrooms');
                    }
                    
                    if (stepKid==='1') {
                        jQuery('span.children-label-wrapper').text('Child');
                    } else {
                        jQuery('.children-label-wrapper').text('Children');
                    }

                    // set text
                    numRooms.text(keyRoom);
                    
                    // if (stepRoom==='10' || stepKid==='10' || stepAdult==='8'){
                    //     jQuery('.parent-list').css('font-size','12px');
                    // }else{
                    //     jQuery('.parent-list').css('font-size','12px');
                    // }

                    numAdults.text(stepAdult);
                    numKids.text(stepKid);
                  }
                });
            });


        },
        megaMenu: function() {
            
            jQuery('#mega-menu-primary li.mega-menu-item-has-children>a').each(function(){
                jQuery(this).removeAttr('href');
            });

            var megaToggle = jQuery('#mega-menu-primary li a'),
            href;

            megaToggle.on('click',function(e){
                //if(jQuery('body').hasClass('is-mobile')){
                    if(jQuery(this).parent().hasClass('mega-menu-item-has-children') ){
                        if(!jQuery(this).parent().hasClass('open')){
                            jQuery(this).parent().addClass('open');
                            return false;
                        }else{
                            jQuery(this).parent().removeClass('open');
                        }
                    }                    
                //}

               else if(!jQuery(this).parent().is('#mega-menu-item-152')){
                    /*disable ssc gallery on megamenu*/
                    if(jQuery(this).parent().is('#mega-menu-item-1446')){                    
                        href = jQuery(this).attr('href')+"?ssc_disable=1";
                        console.log(href + " disable ss-combiner for this page only !");
                        location.href = href;
                    }else{
                        href = jQuery(this).attr('href');
                        location.href = href;
                    }                
                }
            });
        },
        stickySearch: function() {
            
            var searchContainer = jQuery('#home-top').find('.sticky-search-container'),
            subSearchContainer = jQuery('#custom-select-step'),
            theOffset = jQuery('#home-middle');

            if (!jQuery('body').hasClass('home')) { return false; }        

            jQuery(window).scroll(function(){

                var hT = theOffset.offset().top;

                if(jQuery(this).scrollTop() > hT){
                    jQuery('body').addClass('sticky-search-on');
                    searchContainer.addClass('sticky-search search');
                    jQuery('#ui-datepicker-div').addClass('search');
                    subSearchContainer.addClass('sticky-sub-search search');  
                }
                else{
                    jQuery('body').removeClass('sticky-search-on');
                    searchContainer.removeClass('sticky-search search mobile-sticky');
                    jQuery('#ui-datepicker-div').removeClass('search');
                    subSearchContainer.removeClass('sticky-sub-search search');
                }
            });

            searchContainer.find('form').click(function(){
                searchContainer.addClass('mobile-sticky');
            });

            jQuery(document).mouseup(function (e){

                var container = jQuery('.search');

                if (!container.is(e.target) && container.has(e.target).length === 0){
                    searchContainer.removeClass('mobile-sticky');
                }

            });

        },
        searchResultFilter: function() {
            if(jQuery('body').hasClass('page-id-116')){
                filterPrepend();
            }else{
                return false;
            }

            function filterPrepend(){
                var filterForm = jQuery('.search-param');
                
                jQuery('#content').prepend(filterForm);
                var replaceDiv = filterForm.next();

                if(replaceDiv.length !== 0){
                    replaceDiv.insertBefore(filterForm);
                    filterForm.addClass('container');
                    filterForm.wrapAll('<div class="filter-parent"></div>');
                }
                
            }
        },
        serviceMore: function() {
            var clicK = 0;
            jQuery('#read-more-services-trigger').click(function(e){            
                e.preventDefault();
                
                    clicK++;
                    switch(clicK) {
                        case 1:
                            jQuery(this).html('Read Less');
                            jQuery(this).parent().fadeOut(300,function(){
                                jQuery(this).fadeIn(300);
                                jQuery(this).addClass('more');
                            });
                            break;
                        case 2:
                            jQuery(this).html('Read More');
                            jQuery(this).parent().fadeOut(300,function(){
                                jQuery(this).fadeIn(300);
                                jQuery(this).removeClass('more');
                            });     
                            clicK = 0;
                            break;
                    }           
            });

            var custReview = jQuery('.customer-review'),
            custChild = custReview.find('.review-wrap');

            if (custChild.height() < 457) {
                custReview.find('.view-all').hide();
            }else{
                custReview.find('.view-all').show();
            }

            var custService = jQuery('.services'),
            serChild = custService.find('.service-wrap');

            if (serChild.height() < 200) {
                custService.find('.view-all').hide();
            }else{
                custService.find('.view-all').show();
            }
        },
        vrBug : function() {

            function getRootUrl() {
                return window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/';
            }

            var vrShow = jQuery('.vr-icon, .btn-vr');
            var panO = jQuery('.vr-content');

            // VR function
            panO.append('<i class="fa fa-times-circle close" aria-hidden="true"></i>');     

            vrShow.click(function(){

                // prepare variables
                var vr_index = jQuery(this).attr('data-index') ? jQuery(this).attr('data-index') : 0;
                var vr_prev = jQuery('.btn-prev');
                var vr_next = jQuery('.btn-next');
                // remove old vr data if any
                jQuery('.vr-content').children().not('.fa-times-circle.close, .btn-vr').remove();

                // prepare ajax data
                var vr_ajaxdata = {
                    'action': 'ss_single_villa_vr',
                    'post_id' : jQuery('input[name="post_id"]').val(),
                    'index' : vr_index
                };

                panO.animate({top:'0px',opacity:'1'},300,function(){
                    jQuery(this).css({"pointer-events":"auto"});
                });
               
                // pass url value to AJAX
                jQuery.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: vr_ajaxdata,
                    success: function(response) {
                        panO.append(response);
                        console.log('ajax loading');
                        console.log(this.url+" "+this.data);
                        // handle button
                        if (vr_index > 0 ) { 
                            vr_next.hide(); 
                            vr_prev.text('Click to view '+jQuery('input[name="vr-prev"]').val());
                            vr_prev.show();
                        } else {
                            vr_prev.hide(); 
                            vr_next.text('Click to view '+jQuery('input[name="vr-next"]').val());
                            vr_next.show();
                        }
                    },
                    complete: function() {
                        panO.find('.ggskin_text').html('<div class="load-ajax"><img src="'+getRootUrl()+'wp-content/uploads/2016/08/loading1.gif" alt="loading..."></div>');
                        console.log('ajax loaded');

                    },
                    error: function() {
                        panO.html("<div class='error-ajax'>Error load VR</div>");
                        console.log('ajax error');
                    }
                });
            });
            
            //close VR
            jQuery('.close').click(function(){
                panO.animate({top:'-1000px',opacity:'0'},300,function(){
                    jQuery(this).css({"pointer-events":"none"});
                });
            });
        },
        sideBarFix : function() {  

            if(jQuery('body').hasClass('page-id-79') || jQuery('body').hasClass('blog') && jQuery('body').hasClass('not-mobile') || jQuery('body').hasClass('single-post') && jQuery('body').hasClass('not-mobile')){
                var sideBar = jQuery('.sticky-bar'),
                entryRev = jQuery('.sticky-target');

                if(jQuery('#page-sidebar').length){
                    sideBar.find('aside').wrapAll('<div>');
                }
                

                jQuery(window).scroll(function(){

                    if(jQuery(this).scrollTop() > entryRev.offset().top && jQuery(this).scrollTop() < (entryRev.height() + entryRev.offset().top) - 700){
                        sideBar.addClass('scroll-active');
                        sideBar.css({
                            '-ms-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            '-o-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            '-moz-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            '-webkit-transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)',
                            'transform':'translateY('+ (jQuery(this).scrollTop() - entryRev.offset().top + 10) +'px)'
                        });                   
                    }
                    else if(jQuery(this).scrollTop() > entryRev.offset().top && jQuery(this).scrollTop() > (entryRev.height() + entryRev.offset().top) - 700){
                        sideBar.removeClass('scroll-active');
                        sideBar.css({
                            '-ms-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            '-o-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            '-moz-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            '-webkit-transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)',
                            'transform':'translateY('+ (entryRev.height() - sideBar.height()) +'px)'
                        });
                    }
                    else{
                        sideBar.removeClass('scroll-active');
                        sideBar.css({
                            '-ms-transform':'translateY(0)',
                            '-o-transform':'translateY(0)',
                            '-moz-transform':'translateY(0)',
                            '-webkit-transform':'translateY(0)',
                            'transform':'translateY(0)'
                        });
                    }
                });
            }else{
                return false;
            }
        },
        mapHighlight: function() {
            $.fn.maphilight.defaults = {
                fill: false,
                fillColor: 'ffffff',
                fillOpacity: 0.4,
                stroke: false,
                strokeColor: 'transparent',
                strokeOpacity: 1,
                strokeWidth: 2,
                fade: true,
                alwaysOn: false,
                neverOn: false,
                groupBy: false,
                wrapClass: true,
                shadow: true,
                shadowX: 0,
                shadowY: 0,
                shadowRadius: 15,
                shadowColor: 'ffffff',
                shadowOpacity: 1,
                shadowPosition: 'outside',
                shadowFrom: false
            }

            //init interactive map
            jQuery('img[usemap]').maphilight();

            var mapImage = jQuery('#ImageMapsCom-villakubu-floorplan'),
            mapArea = mapImage.find('area'),
            tolltipParent = jQuery('.floorplan-container'),
            tooltipContainer = jQuery('.floorplan-container>div'),
            tooltipActive = jQuery('.floorplan-container .active-hover');

            tooltipContainer.prepend('<i class="fa fa-times-circle hover-close"></i>');
            tooltipContainer.prepend('<i class="fa fa-expand resize"></i>');

            //larger screen for dragable
            if(jQuery(window).width() > 767){
                tolltipParent.draggable({
                    cursor: 'move',
                    containment: 'document',
                    snap: '#content',
                    start: function () {
                        tolltipParent.css({right:'auto'});
                    }
                });
            }

            mapArea.on('mouseenter',function(){
                var thisAttr = jQuery(this).attr('data-hover');
                hideEl();
                jQuery('#'+thisAttr).addClass('active-hover');
            });

            jQuery('.resize').on('click',resizeEl);

            jQuery('.hover-close').on('click',hideEl);

            jQuery(document).mouseup(function (e){
                var container = jQuery('.floorplan-container>div');
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    hideEl();
                }
            });

            //resize dialog
            function resizeEl() {
                tolltipParent.toggleClass('zoomed');
                tooltipContainer.find('img').toggleClass('zoomed');
            }
            
            //hide dialog
            function hideEl() {
                tooltipContainer.removeClass('active-hover');
                tolltipParent.css({top:'0',left:'auto',right:'0'});
                tolltipParent.removeClass('zoomed');
                tooltipContainer.find('img').removeClass('zoomed');
            }

            //force canvas height
            jQuery('.vl-floorplan-layout canvas').css({
                height : 'auto'
            });
        },
        inquiryStyling: function() {
            if(jQuery('body').hasClass('page-id-1057')){
                // jQuery('#post-1057 #bapi-inquiryform').addClass('col-sm-10');
                jQuery('.flex-direction-nav>li>a').empty();
            }else{
                return false;
            }
        },
        buttonSearch: function() {
            if(jQuery('body').hasClass('page-template-template_search_villas')){
                jQuery('.filter>div>form').prepend('<div class="button-category"></div>');
                var buttonFilter = jQuery('.field-wrapper:not(:last-child)');

                buttonFilter.show(1,function(){
                    jQuery(this).appendTo(".button-category");
                    jQuery(this).css('display','inline-block');
                });
                

            }else{
                return false;
            }
        },
        fixBugModal: function() {

            function dialogTrigger(el){
                var thisEl = jQuery(el);
                var trigger = jQuery('*[data-toggle="ss-modal"]');
                var close = thisEl.find('.modal-close');

                thisEl.on('show.bs.modal',function(){
                    if(!jQuery('body').hasClass('ss-modal-show')){
                        jQuery('body').addClass('ss-modal-show');
                    }                
                });

                thisEl.on('hide.bs.modal',function(){
                    jQuery('body').removeClass('ss-modal-show');
                });

                trigger.live('click',function(e){
                    e.preventDefault();
                    var target = jQuery(this).attr('data-target');
                    var dialog = jQuery('.ss-modal-dialog').find(target).parent();
                    if(!dialog.hasClass('active')){
                        dialog.addClass('active');
                        thisEl.trigger('show.bs.modal',this);
                    }else{
                        return false;
                    }
                });

                close.live('click',function(){
                    jQuery(this).parents('.ss-modal-dialog').removeClass('active');
                    thisEl.trigger('hide.bs.modal',this);
                });

                jQuery(document).mouseup(function (e){

                    var container = jQuery('.ss-modal');

                    if(container.attr('id') === "enquireModal"){
                        return false;
                    }

                    if (!container.is(e.target) && container.has(e.target).length === 0){
                        jQuery('.ss-modal-dialog').removeClass('active');
                        thisEl.trigger('hide.bs.modal',this);
                    }

                });
            };        

            dialogTrigger('.ss-modal');

            //AJAX delegate
            var bapiInit = false;
            jQuery("#bookingform").on("DOMSubtreeModified",function(){
              if (!bapiInit) {
        
                    setTimeout(function() {
                        var elm = jQuery('#revisedates').parent();
                        var checkIn = elm.find('#makebookingcheckin');

                        if(checkIn.val() == ""){
                            if(!elm.hasClass('active')){
                                elm.addClass('active');
                                elm.trigger('show.bs.modal',this);
                            }else{
                                    return false;
                            }
                        }else{
                            return false;
                        }

                         
                    }, 1000);
                
                bapiInit = true;
              }
            });
        },
        initHoverdir: function() {

            if(jQuery('body').hasClass('page-id-64')){
                
                jQuery('.gallery>dl>dt').find('a').append('<div>');
                            
                jQuery('.gallery>dl>dt').each(function(){
                    jQuery(this).hoverdir({
                        speed: 150,
                        easing: 'ease',
                        hoverDelay: 0
                    });
                });

            }else{
                return false;
            }
        },
        contactUs: function() {


            if(jQuery('body').hasClass('page-id-347')){

                var bapiContain = jQuery('#bapi-inquiryform'),
                ul = bapiContain.find(".unstyled"),
                input = ul.find("li>input.required");

                input.on('focus',function(){
                    jQuery(this).removeClass('error');
                    jQuery(this).parent().removeClass('error');
                });

                input.on('blur',function(){
                    if(jQuery(this).val()){
                        jQuery(this).removeClass('error');
                        jQuery(this).parent().removeClass('error');
                    }                
                });

                jQuery('#btn-leadrequest').click(function(e) {
                    var isValid = true;
                    input.each(function() {
                        if (jQuery.trim(jQuery(this).val()) == '') {
                            isValid = false;
                            jQuery(this).addClass('error');
                            jQuery(this).parent().addClass('error');
                        }
                        else {
                            jQuery(this).removeClass('error');
                            jQuery(this).parent().removeClass('error');
                        }
                    });
                    if (isValid == false){
                        e.preventDefault();                   
                    }
                    
                    ga('send', {
                          hitType: 'event',
                          eventCategory: 'Enquiry Form',
                          eventAction: 'enquiryclick',
                          eventLabel: 'Enquiry sent'
                        });
                    
                    //ga('send', 'event', 'Enquiry Form', 'enquiryclick', 'Enquiry sent');

                    var email = jQuery("input#txtEmail").val();  
                    if (!validEmail(email)) {
                        jQuery("input#txtEmail").addClass('error');
                        jQuery("input#txtEmail").parent().addClass('error'); 
                    } 

                });
            }else{
                return false;
            }
        },
        footerSscDisable: function() {
            var nossc = jQuery('#menu-item-82>a');
            var nosscHref; 
            /*disable ssc gallery on footer*/
            nossc.on('click',function(e){
                return false;
                nosscHref = nossc.attr('href')+"?ssc_disable=1";
                console.log(nosscHref + " disable ss-combiner for this page only !");
                location.href = nosscHref;
            });
        },
        loginFunction: function() {
            if (jQuery('body').hasClass('page-id-2149')){
                jQuery('form#loginform input[type="text"]').attr('placeholder','Username or Email');
                jQuery('form#loginform input[type="password"]').attr('placeholder','Password');

                var input = jQuery('form#loginform input:not([type="submit"])');

                input.on('focus',function(){
                    jQuery(this).parent().removeClass('error');
                });

                input.on('blur',function(){
                    if(jQuery(this).val()){
                        jQuery(this).parent().removeClass('error');
                    }                
                });

                jQuery('#wp-submit').click(function(e) {
                    
                    var isValid = true;
                    input.each(function() {
                        if (jQuery.trim(jQuery(this).val()) == '') {
                            isValid = false;
                            jQuery(this).parent().delay(1000).addClass('error');
                        }
                        else {
                            jQuery(this).parent().delay(1000).removeClass('error');
                        }
                    });
                    if (isValid == false){
                        e.preventDefault();                   
                    } 

                });
            }else{
                return false;
            }
        }
    },
/*===================================================================================================================================

END custom.js

===================================================================================================================================*/
//
//
//
//
//
//
//
//
//
//

/*===================================================================================================================================

Nasser Maronie
Villakubu
SoftwareSeni 2016

contrib: Nasser
custom-new.js
===================================================================================================================================*/
    newCustom : {
        init: function($){
            Villakubu.newCustom.datePickerModal();
            Villakubu.newCustom.enquireAgent();
            Villakubu.newCustom.inquiryWhiteBg();
            Villakubu.newCustom.bugReview();
            Villakubu.newCustom.bugDialogInquirySlider();
            Villakubu.newCustom.wrapInquiry();
            Villakubu.newCustom.objectFitPolyFill();
            Villakubu.newCustom.dropdownCurrency();
        },
        datePickerModal: function() {
            if(jQuery("body").hasClass("page-template-template_agent")){
                            
                jQuery('.page-template-template_agent .btn-enquire').live('click',function(e){
                    var targetModal = jQuery(this).attr('data-target');

                    jQuery("#ui-datepicker-div").appendTo(targetModal);
                    
                });
            }else{
                return false;
            }
        },
        enquireAgent: function() {
            jQuery('#agentInquiry').appendTo('.availability-calendar .container .luxelet-calendar');
        },
        inquiryWhiteBg: function() {
            if(jQuery('body').hasClass('page-id-1057')){
                if(jQuery('.entry').find('.enquiry-form').length == 1){
                    
                    jQuery('body.page-id-1057 #content>.container').css({
                        'background':'none',
                        'box-shadow':'none'
                    });

                    jQuery('body.page-id-1057 #content').css({
                        'background':'#fff'
                    });

                }else{
                    return false;
                }
            }else{
                return false;
            }
        },
        bugReview: function() {//this happen if the page being translate with translator plugin
            var reviewContainer = jQuery('#archivements .award-container>.award');

            //AJAX delegate
                var translateInit = false;
                reviewContainer.on("DOMSubtreeModified",function(){
                  if (!translateInit) {
         
                        setTimeout(function() {
                            console.log('translated')
                            
                            reviewContainer.each(function(){
                                var len = jQuery(this).find('a');
                                len.appendTo(jQuery(this));
                            });


                        }, 1000);
                    
                    translateInit = true;
                  }
                });     
        },
        bugDialogInquirySlider: function() {

            jQuery(document).mouseup(function (e){

                var dialogError = jQuery('#fullScreenSlideshow>.modal-dialog');

                    if (!dialogError.is(e.target) && dialogError.has(e.target).length === 0){
                        
                        dialogError.parent().modal('hide');                   
                        
                    }

            });
        },
        wrapInquiry: function() {
            if(!jQuery('#post-1057').length){
                return false
            }

            jQuery("#post-1057 .entry>div[class*='col-']").wrapAll('<div class="booking-page">');

            jQuery('#post-1057 .entry .booking-page').append('<div class="clearfix"></div>')
        },
        objectFitPolyFill: function () {
            objectFitImages();
        },
        dropdownGtranslate: function() {

            

        },
        dropdownCurrency: function() {
            jQuery('#ss_part_widget-15 .currencyselector .dropdown-menu > li >a').click(function(){
                jQuery('#ss_part_widget-15 .currencyselector .dropdown-menu > li >a.current-currency').removeClass('current-currency');
                jQuery(this).addClass('current-currency');
            });
        }
    },
/*===================================================================================================================================

END custom-new.js

===================================================================================================================================*/
//
//
//
//
//
//
//
//
//
//

	handleGtranslate: function() {

		(function($){
			$.fn.gtranslate = function(option){
			    
		        var settings = $.extend({
		        		source: $("#gtranslate"),
			            fakeDropdown: $("<div></div>"),
				    	callback: function(){}//callback
		        }, option);

		        var googtrans = getCookie("googtrans");

		        if (googtrans) {
		          googtrans = googtrans.split('/')[2];
		        } else {
		          googtrans = settings.source.val().split('|')[1];
		        }

		        settings.source.val('en|'+googtrans);

		        var selected = settings.source.find("option[value='en|"+googtrans+"']"),
		        	options = $("option", settings.source);

		        return this.each(function(){  
		        	var el = $(this);
		        	el.append(settings.fakeDropdown);

		        	settings.fakeDropdown.append(
		        		'<a>'+
		        			'<span class="gtranslate-label">Language</span><img src="'+app.homeURL+'/wp-content/themes/boost/img/flag/' + googtrans + '.png" /><span class="value">' + selected.val() + '</span><i class="fa fa-chevron-down"></i>'+	        			
		        		'</a>'+
		        		'<ul></ul>');
		        	options.each(function(){
        		    	settings.fakeDropdown.find('ul').append('<li><a><img src="'+app.homeURL+'/wp-content/themes/boost/img/flag/'+$(this).val().split('|')[1]+'.png" />' + 
        		        '<span>'+ $(this).text() +'</span>'+'<span class="value notranslate">' + 
        		        $(this).val() + '</span></a></li>');
        		    });
        		    settings.fakeDropdown.find("a:first").click(function() {
        		        $(this).parent().find("ul").fadeToggle(200);
        		    });
        		    settings.fakeDropdown.find("ul").find("a").click(function(){
        		    	$("ul li a.current-lang").removeClass("current-lang");
        		    	$(this).addClass("current-lang");
        		    	console.log(googtrans)
        		    	settings.callback();
        		    });
        		    $(document).mouseup(function (e){
        		        var dropdown = settings.fakeDropdown.find("ul");
    		            if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0){        		                
    		                dropdown.fadeOut(300); 
    		            }
        		    });
		        });		        
			}
		}(jQuery));

		$("#gtranslate").gtranslate({
			source: $("#gtranslate select:first"),
			fakeDropdown : 	$('<div id="target" class="dropdown"></div>'),
			callback: function(){
				console.log('callback');
			}
		});
		$("#gtranslate > .dropdown > ul > li > a").click(function() {
		   var text = '<span class="gtranslate-label">Language</span>'+ $(this).html() +'<i class="fa fa-chevron-down"></i>';
		   $("#gtranslate .dropdown > a").html(text);
		   $("#gtranslate .dropdown > ul").fadeOut(200);
		    
		   var source = $("#gtranslate select:first");
		   var new_lang = $(this).find("span.value").html();
		   if (source.val()!=new_lang) {
		     source.val(new_lang).change();
		   }
		});
	},
	widgetOnSearchPosition: function(){
		(function($){

			$.fn.responsivePosition = function(option){
				var settings = $.extend({
					offsetTarget: jQuery('.ss-villa-search-widget'),
					elTarget: jQuery('#custom-select-step'),
					onDesktop: 400,
					onMobile: 200,
					elCloseToBot: {},
					elCloseToTop: {},
					elOnStick: {},
					stickySearch: null,
					targetSticky: null
				}, option);

							

				return this.each(function(){			
					if(settings.targetSticky){
						var sticky_target = settings.targetSticky;
					}

					var desktop = settings.offsetTarget.offset().top > (jQuery(window).scrollTop() + jQuery(window).height() - settings.onDesktop),					
						mobile = settings.offsetTarget.offset().top > (jQuery(window).scrollTop() + jQuery(window).height() - settings.onMobile);

					if(jQuery('body').hasClass('is-mobile')){
						if(mobile){	
							settings.elTarget.css(settings.elCloseToBot);
						}else{
							settings.elTarget.css(settings.elCloseToTop);							
							if(settings.stickySearch == true){
								if(sticky_target){settings.elTarget.css(settings.elOnStick)}
								else{settings.elTarget.css(settings.elCloseToTop);}
							}							
						}						
					}else{
						if(desktop){
							settings.elTarget.css(settings.elCloseToBot);
						}else{
							settings.elTarget.css(settings.elCloseToTop);				
							if(settings.stickySearch == true){
								if(sticky_target){settings.elTarget.css(settings.elOnStick)}
								else{settings.elTarget.css(settings.elCloseToTop);}
							}							
						}
					}
				});
			}
		}(jQuery));

		var selectStep = jQuery('#custom-select-step'),
			targetSelectStep = jQuery('.ss-villa-search-widget .search-dropdown');

		targetSelectStep.on('click',function(){
			jQuery(this).responsivePosition({	
				elCloseToBot:{
					'top':targetSelectStep.offset().top - selectStep.outerHeight(),
			        'left':targetSelectStep.offset().left +10.5,
			        'width':targetSelectStep.width()
				},
				elCloseToTop:{
					'top':targetSelectStep.offset().top +38,
			        'left':targetSelectStep.offset().left +10.5,
			        'width':targetSelectStep.width()
				},
				stickySearch: true,
				targetSticky: targetSelectStep.parents('.sticky-search-container').hasClass('sticky-search'),
				elOnStick: {
					'top':(targetSelectStep.offset().top +38) - jQuery(window).scrollTop(),
			        'left':targetSelectStep.offset().left +10.5,
			        'width':targetSelectStep.width()
				}
			});
		});
		
		jQuery('#gtranslate .dropdown a:first').on('click',function(){
			jQuery(this).responsivePosition({
				elTarget: jQuery('#gtranslate .dropdown ul'),			
				elCloseToBot:{'top':'auto','bottom':'30px'},
				elCloseToTop:{'top':'27.5px','bottom':'auto'},
				stickySearch: false
			});
		});
		
		jQuery('.currency-wrapper .dropdown-toggle').on('click',function(){
			jQuery(this).responsivePosition({
				elTarget: jQuery('.currency-wrapper .dropdown-menu'),	
				elCloseToBot:{'top':'auto','bottom':'26px'},
				elCloseToTop:{'top':'27px','bottom':'auto'},
				stickySearch: false
			});
		});
	},
endVillakubu: "" //end
};



jQuery(document).ready(function($) {
    Villakubu.init($);
});

/*===================================================================================================================================


===================================================================================================================================*/